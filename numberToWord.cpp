
#include <iostream>
#include <string>
using namespace std;
const string EMPTY = "";
// bo phan tu dau tien de de  index
const string X[] = { EMPTY, "One ", "Two ", "Three ", "Four ", "Five ", "Six ", "Seven ", "Eight ", "Nine ", "Ten ", "Eleven ","Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
 // bo 2 phan tu dau tien de de index
const string Y[] = { EMPTY, EMPTY, "Twenty ", "Thirty ", "Forty ", "Fifty ","Sixty ", "Seventy ", "Eighty ", "Ninety " };
                
                
// check so hop le, neu la so thap phan tra them vi tri dau .
struct CheckInput{
    short int  kq; //dinh dang sai 0; dung:  so am -1;so duong 1
    int pos; // vi tri dau "."
};

CheckInput check(string a){
        CheckInput ans;
        
        // phan thu dau tien khac [0 9] va khac dau "-" ==> 0
        if( (a[0] < 48 || a[0] > 57) && (a[0] != 45) ){
           
            ans.kq = 0;
            return ans;
        }
        // phan tu dau tien la "-", phan tu thu 2 khac so==>0
        if(a[0] == 45){
            
            if( (a[1] < 48) || (a[1] > 57) ){
                ans.kq = 0;
                return ans;
            }
        }
        
        int numDot = 0; // so luong dau '.'
        for(int i = 1; i < a.length() - 1; i++){
            // tu phan tu thu 2->n-1  : khac [0 9] va '.' ==> 0
            if( ((a[i] < 48) || (a[i] > 57)) && (a[i] != 46) ){
                ans.kq = 0;
                return ans;
            }
            
            if(a[i] == 46){
                ans.pos = i; // ghi vi tri dau "." dau tien
                numDot++; // dem "."
            }
            
            // so dau cham > 1 => 0
            if(numDot > 1){
                //cout<<(numDot)<<endl;
                ans.kq = 0;
                return ans;
            }
            
            
        }
        // phan tu cuoi khong phai so ==> 0
        if((a.back() < 48) || (a.back() > 57)) {
            ans.kq = 0;
            return ans;
        }
        // khong co dau "." => pos"cham" = -1
        if(numDot == 0){
            ans.pos = -1;   
        }
        
        // so am thi => -1 ; duong thi => 1
        if(a[0] == 45){
            ans.kq = -1;
        }
        else {
            ans.kq = 1;
        }
        
return ans;
}

// Chuyen so co mot chu so va 2 chu so
string convertToDigit(int n, string suffix)
{
    // if `n` is zero
    if (n == 0) {
        return EMPTY;
    }
 
    // split `n` if it is more than 19
    if (n > 19) {
        return Y[n / 10] + X[n % 10] + suffix;
    }
    else {
        return X[n] + suffix;
    }
}
 
// Function to convert a given number (max 9-digits) into words
string convert(unsigned long long int n){
    // string to store word representation of the given number
    string res;
 
    // this handles digits at ones and tens place
    res = convertToDigit((n % 100), "");
 
    if (n > 100 && n % 100) {
        res = "and " + res;
    }
 
    // this handles digit at hundred place
    res = convertToDigit(((n / 100) % 10), "Hundred ") + res;
 
    // this handles digits at thousand and tens thousand place
    res = convertToDigit(((n / 1000) % 100), "Thousand ") + res;
 
    // this handles digits at hundred thousand and one million place
    res = convertToDigit(((n / 100000) % 100), "Lakh, ") + res;
 
    // this handles digits at ten million and hundred million place
    res = convertToDigit((n / 10000000) % 100, "Crore, ") + res;
 
    // this handles digits at ten million and hundred million place
    res = convertToDigit((n / 1000000000) % 100, "Billion, ") + res;
 
    return res;
}
    
int main() {
    string a;
    cout << ("Chuong trinh doi so ra chu Tieng anh (max 9 chu so) ")<< endl;
    cout <<("Moi ban nhap so,neu la so thap phan, vui long go ki tu . : n = ");
    cin >> a;
    while((check(a).kq == 0)|| (a.length() > 9)){
        cout <<("Dinh dang so khong dung\nHoac so qua dai\nMoi ban nhap so,neu la so thap phan, vui long go ki tu . : \nn = ");
        cin >>a;
    }
    /* kq = 0 sai dinh dang
        kq = 1 so duong 
        kq = -1 so am
        pos = -1 so nguyen
        pos > 0 so thap phan , dau "." tai vitri pos
    */
    int kq   = check(a).kq; 
    int pos  = check(a).pos;
    
    if (kq == 1){
        
        // neu la nguyen
        if (pos == -1){
          cout << convert(stoi(a)) << endl;
        }
        
        else{ // neu la so thap phan duong
            
            string str1 = a.substr(0,pos);
            string str2 = a.substr(pos + 1 );
            cout<< convert(stoi(str1));
            cout<<" dot ";
            for(int i = 0; i < str2.length(); i++){
                if(str2[i] - '0' == 0){
                    cout<<"Zezo ";
                }
                else{            
                cout << convert(str2[i] - '0') ;
                }
            }
        }
    }
    // neu la so am
    else{
        // neu la so nguyen am
        if (pos == -1){
            cout<<"Negative ";
            cout << convert(stoi(a.substr(1))) << endl;
        }
            // neu la so thap phan am
        else{
            string str1 = a.substr(1,pos);
            string str2 = a.substr(pos + 1 );
            cout<<"Negative ";
            cout<< convert(stoi(str1));
            cout<<" dot ";
            for(int i = 0; i < str2.length(); i++){
                if(str2[i] - '0' == 0){
                    cout<<"Zezo ";
                }
                else{            
                cout << convert(str2[i] - '0') ;
                }
            }
        }
    }
    
    if(a == "0"){
        cout<<"Zezo"<<endl;
    }
    cout <<"\n";
    
   
    
    cout << "end";

    return 0;
}